import json
import urllib
import urllib2

class ApiUtil:
	@classmethod
	def post(cls, url, body):
		data = urllib.urlencode(body)
		req = urllib2.Request(url, data)
		response = urllib2.urlopen(req)
		raw = response.read()
		return json.loads(raw)
