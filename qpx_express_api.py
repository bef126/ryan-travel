import secret
from api_util import ApiUtil

class Slice:
	def __init__(self):
		self.QUERY = {}

	def add_required(self, origin, destination, date):
		"""
		origin -- airport code
		destination -- airport code
		date -- string in format YYYY-MM-DD
		"""

		self.QUERY["origin"] = origin
		self.QUERY["destination"] = destination
		self.QUERY["date"] = date

	def set_earliest_departure_time(self, time):
		""" time -- string in format HH:MM """

		if self.QUERY["permittedDepartureTime"]:
			self.QUERY["permittedDepartureTime"]["earliestTime"] = time
		else:
			self.QUERY["permittedDepartureTime"] = {"earliestTime": time}

	def set_latest_departure_time(self, time):
		""" time -- string in format HH:MM """

		if self.QUERY["permittedDepartureTime"]:
			self.QUERY["permittedDepartureTime"]["latestTime"] = time
		else:
			self.QUERY["permittedDepartureTime"] = {"latestTime": time}

	def set_max_stops(self, number):
		""" number -- integer """
		
		self.QUERY["maxStops"] = number

	def set_max_connection_duration_in_minutes(self, number):
		""" number -- integer """

		self.QUERY["maxConnectionDuration"] = number

	def set_preferred_cabin(self, string):
		""" string -- one of ["COACH", "PREMIUM_COACH", "BUSINESS", "FIRST"] """

		self.QUERY["preferredCabin"] = number

	def add_permitted_carrier(self, string):
		""" string -- the 2-letter IATA airline designator """

		if self.QUERY["permittedCarrier"]:
			self.QUERY["permittedCarrier"].append(string)
		else:
			self.QUERY["permittedCarrier"] = [string]

	def add_prohibited_carrier(self, string):
		""" string -- the 2-letter IATA airline designator """

		if self.QUERY["prohibitedCarrier"]:
			self.QUERY["prohibitedCarrier"].append(string)
		else:
			self.QUERY["prohibitedCarrier"] = [string]

	def set_alliance(self, string):
		""" string -- one of ["ONEWORLD", "SKYTEAM", "STAR"] """

		self.QUERY["alliance"] = string

class QPXExpressRequest:
	def __init__(self, key):
		self.API_KEY = key
		self.BASE_URL = 'https://www.googleapis.com/qpxExpress/v1/trips'
		self.QUERY = {"saleCountry": "US"}

	def set_max_results(self, number):
		""" number -- integer, max value of 200 """

		self.QUERY["solutions"] = number

	def set_max_price(self, number):
		""" number -- float in US Dollars """

		self.QUERY["maxPrice"] = "USD" + str(number)

	def set_adult_passengers(self, number):
		""" number -- integer """

		self.QUERY["passengers"] = {"adultCount": number}

	def add_slice(self, slice):
		""" slice -- Slice() object (see documentation above for slice) """

		if self.QUERY["slice"]:
			self.QUERY["slice"].append(slice)
		else:
			self.QUERY["slice"] = [slice]

	def search(self):
		url = self.BASE_URL + "/search?key=" + self.API_KEY
		result = ApiUtil.post(self, url, {"request": self.QUERY})
		return QPXExpressResponse(result)

class QPXExpressResponse:
	def __init__(self, json):
		self.JSON = json

	def request_filter(self, fn):
		""" filter the result set based on a lambda """

		results = self.JSON["trips"]["tripOption"]
		self.JSON["trips"]["tripOption"] = filter(fn, results)

	def filter_by_booking_code(self, code):
		""" only return results where bookingCode == code """
		
		fn = lambda x: x["slice"][0]["segment"][0]["bookingCode"] == code
		self.request_filter(fn)

	def request_id(self):
		return self.JSON["trips"]["requestId"]
