from qpx_express_api import *
import json

def load_json(fname):
	""" load a file from disk and parse it into a json dict """
	
	f = open(fname, "r")
	raw = f.read()
	f.close()
	return json.loads(raw)

# load from disk so we can test without burning through our quota
r = QPXExpressResponse(load_json('example_response'))
r.filter_by_booking_code("W")

for t in r.JSON["trips"]["tripOption"]:
	# since we filtered by "W", we should only see "W"
	print t["slice"][0]["segment"][0]["bookingCode"]